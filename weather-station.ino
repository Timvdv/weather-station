/**
 * Tim van de Vathorst
 * Weather station
 */

#include <SimpleDHT.h>
#include <SPI.h>
#include <Wire.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

WiFiMulti wifiMulti;
Adafruit_BMP280 bme; // I2C

boolean ble_scan_finished = false;

// DHT CODE
const int DHTPin = 14;
SimpleDHT22 dht22;
float temperature = 0;
float humidity = 0;

// Avoid sensor
const int avoidSensorPin = 32;
boolean enableDisplay = false;
boolean writeIp = true;

// Setup looping without loop
unsigned long previousMillis = 0;
const long interval = 7000;
int minute_counter = 0;

// Init outdoor values on 0
int outside_wind = 0;
int outside_light = 0;
float outside_temp = 00.00;
float outside_humidity = 00.00;
float kilo_pascal = 00.00;
String wind_direction = "-";
              
void setup() {
    pinMode (avoidSensorPin, INPUT);

    Serial.begin(115200);
    while(!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
    
    Serial.println();
    Serial.println();
    Serial.println();

    // Initialize DF Player for sound
    initDfPlayer();

    // Initialize OLED display
    initLedDisplay();

    // Draw loading screen
    drawText("loading...", 1);

    wifiMulti.addAP("xx", "xx");

    // Om dit werkend te krijgen moest in nog wat aanpassen in de lib -> #define BMP280_ADDRESS (0x76)
    if (!bme.begin()) {  
      Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    }

    
    // Initialize WiFi
    if(wifiMulti.run() == WL_CONNECTED) {
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
    }

    // Make sure WiFi is initialized before starting Bluetooth
    delay(7000);

    // Start Bluetooth search for outdoor sensor values
    if (btStart()) {
      gattc_client_test();
    }

    startBleScan();

}

void loop() {
    unsigned long currentMillis = millis();

    int val = digitalRead (avoidSensorPin);
    
    if (val == LOW)
    {
      if(!enableDisplay) {
        activateDisplay(String(temperature), String(humidity), String(kilo_pascal));
        
        speak();
        
        delay(2000); // Delay should be removed
        
        drawOutdoor(String(outside_temp), String(outside_humidity), String(outside_light), String(outside_wind), "N");
      }
      
      previousMillis = currentMillis;
      enableDisplay = true;
    }

    if (currentMillis - previousMillis >= interval) {
      
      previousMillis = currentMillis;

      minute_counter++;

      if(minute_counter > 6) {
        minute_counter = 0;
        startBleScan();
      }

      // Read DHT values
      int err = SimpleDHTErrSuccess;
      if ((err = dht22.read2(DHTPin, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
        Serial.print("Read DHT22 failed, err="); Serial.println(err);
      }

      kilo_pascal = (float)(bme.readPressure() / 1000);

      // When display is enabled and loop is here, deactivate display
      if(enableDisplay) {
        
        Serial.println("deactivating display");
        delay(2000); // Delay should be removed
        
        deactivateDisplay();
        enableDisplay = false;
      }

      if(ble_scan_finished) {
        // DO BLE scan after short delay
        ble_scan_finished = false;
      }
      
      printSensorValues();
  
      // wait for WiFi connection
      if((wifiMulti.run() == WL_CONNECTED)) {

          if(writeIp) {
            Serial.println(WiFi.localIP().toString());
            drawText(WiFi.localIP().toString(), 1);  
            writeIp = false;
          }
        
          HTTPClient http;
          http.begin("http://iot-open-server.herokuapp.com/data");
          http.addHeader("Content-Type", "application/json; charset=utf-8");

          String post_string = "{\"token\": \"bfdd2e54ac9f73dffc51df7c\",\"data\": [";

          post_string += "{\"key\": \"temperature\",\"value\": "+ String(temperature) +"},";
          post_string += "{\"key\": \"humidity\",\"value\": "+ String(humidity) +"},";
          post_string += "{\"key\": \"air_pressure\",\"value\": "+ String(kilo_pascal) +"},";
          post_string += "{\"key\": \"outside_temp\",\"value\": "+ String(outside_temp) +"},";
          post_string += "{\"key\": \"outside_humidity\",\"value\": "+ String(outside_humidity) +"},";
          // post_string += "{\"key\": \"wind_direction\",\"value\": \""+ String(wind_direction) +"\"},";
          post_string += "{\"key\": \"outside_wind\",\"value\": "+ String(outside_wind) +"}";

          post_string += "]}";

          int httpCode = http.POST(post_string);

          // Debug post call
          // Serial.println(post_string);
          // Serial.println("HTTP STATUS:");
          // Serial.println(httpCode);
                  
          http.end();
      } else {
        Serial.println("WiFi not connected!");
        delay(5000);
      }
    }
}

void getActuator() {
  
}

void printSensorValues() {
  //  Read BME temp (not used)
  //  Serial.print("Temperature = ");
  //  Serial.print(bme.readTemperature());

  Serial.println(" ");
  Serial.println("Indoor: ");
  Serial.print("Pressure = ");
  Serial.print(kilo_pascal); Serial.print(" kPa, ");
  Serial.print((float)temperature); Serial.print(" *C, ");
  Serial.print((float)humidity); Serial.println(" RH%, ");
  Serial.println("Outdoor: ");
  Serial.print(outside_wind); Serial.print(" km/h "); Serial.print(wind_direction); Serial.print(", ");
  Serial.print(outside_light); Serial.print(" lux, ");
  Serial.print(outside_temp); Serial.print(" *C, ");
  Serial.print(outside_humidity); Serial.println(" RH%");
  Serial.println(" ");  
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
