#include <SimpleDHT.h>
#include "SimpleBLE.h"
#include <stdio.h>

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  3        /* Time ESP32 will go to sleep (in seconds) */

long debouncing_time = 15; //Debouncing Time in Milliseconds
volatile unsigned long last_micros;

// LDR SENSOR CODE
int ldrPin = 34;
int windDirectionPin = 35;
int ldrValue = 0;
int windDirection = 0;

// HALL SENSOR CODE (Magnet)
int hallPin = 12;
int lastHallValue = 0;
int hallValue = 0;
int rounds = 0;

RTC_DATA_ATTR String readableWindDirection = "NONE";

// DHT CODE
const int DHTPin = 14;
SimpleDHT22 dht22;

// Setup looping without loop
unsigned long previousMillis = 0;
unsigned long read_sensor_milis = 0;

//const long interval = 65000;
const long interval = 6000; // Interval used for development
const long read_sensor_interval = 2000; // Interval used for development

// Bluetooth code
String beaconMsg = "";

SimpleBLE ble;

// Sensor values
float temperature = 0;
float humidity = 0;
float windspeed;

int count = 0;

RTC_DATA_ATTR int bootCount = 0;

/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
//void print_wakeup_reason(){
//  esp_deep_sleep_wakeup_cause_t wakeup_reason;
//
//  wakeup_reason = esp_deep_sleep_get_wakeup_cause();
//
//  switch(wakeup_reason)
//  {
//    case 1  : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
//    case 2  : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
//    case 3  : Serial.println("Wakeup caused by timer"); break;
//    case 4  : Serial.println("Wakeup caused by touchpad"); break;
//    case 5  : Serial.println("Wakeup caused by ULP program"); break;
//    default : Serial.println("Wakeup was not caused by deep sleep"); break;
//  }
//}

void setup() {
  Serial.begin(115200);

  pinMode(hallPin, INPUT_PULLUP);
  
  attachInterrupt(digitalPinToInterrupt(hallPin), windSpeedInterrupt, RISING);

  //Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  //Print the wakeup reason for ESP32
  // print_wakeup_reason();

  // pinMode(hallPin, INPUT);

  readDht();
  
  unsigned long currentMillis = millis();

  // LOOP USED FOR DEBUGGING
  //  do {
  //    delay(100);
  //  }
  //  while(millis() < (currentMillis + 60000));

  // Delay is fine because of interrupt
  delay(6000);

  ldrValue = analogRead(ldrPin);
  windDirection = analogRead(windDirectionPin);
  calculateWindSpeed();
  calculateWindDirection();
  readDht();
  printValues();
  createBluetoothNetwork();

  esp_deep_sleep(TIME_TO_SLEEP * uS_TO_S_FACTOR);

  // a delay before shutdown otherwise the packets aren't sent completely
  // the delay required seems to depend on the payload length 
  delay(50); 
}

void loop() {
  // doesn't ever get here
}

void printValues() {
    Serial.print("Temp value: "); Serial.print(temperature);
    Serial.print(", Humid value: "); Serial.println(humidity);
    Serial.print("LDR value: "); Serial.print(ldrValue);
    Serial.print(", Hall value: "); Serial.print(hallValue);
    Serial.print(", Rounds: "); Serial.print(rounds);
    Serial.println(" ");
    Serial.println("----------------------- ");
}

void readDht() {
  //Read DHT
  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(DHTPin, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT22 failed, err=");
    Serial.println(err);
  }
}

void calculateWindSpeed() {

  // FIX ROUNDS
  int fixed_rounds = rounds / 4;
  
  windspeed = (2*3,14*60)*fixed_rounds; // mm/min (2*pi*straal)
  windspeed = windspeed * 10; // from 6 seconds to 60 seconds
  windspeed = windspeed/1000000; // km/min
  windspeed = windspeed*60; // km/hour
  
  if (windspeed > 130) {
    windspeed = 0;
  }

  Serial.print("Read wind speed, speed=");
  Serial.println(windspeed); 
}

void createBluetoothNetwork() {
  
  beaconMsg = "ESP-"+ String(windspeed) + "-" + String(ldrValue) +"-"+ String(humidity) +"-"+ String(temperature) + "-" + readableWindDirection + "-X";
  Serial.println(beaconMsg);
  
  ble.begin(beaconMsg);
  
  delay(2000);
  
  ble.end(); 
}

void windSpeedInterrupt() {
  noInterrupts();
  delayMicroseconds(15000);
  rounds++;
}

void calculateWindDirection() {

  if(windDirection == 0) {
    return;
  }

  if(windDirection > 1700 && windDirection < 2000) {
    readableWindDirection = "N"; 
  }

  if(windDirection > 2000 && windDirection < 2300) {
    readableWindDirection = "NE";
  }

  if(windDirection > 2300 && windDirection < 2500) {
    readableWindDirection = "E";
  }

  if(windDirection > 2500 && windDirection < 2600) {
    readableWindDirection = "SE"; // Werkt niet
  }
  
  if(windDirection > 2600 && windDirection < 2900) {
    readableWindDirection = "S";
  }

  if(windDirection > 2900 && windDirection < 3000) {
    readableWindDirection = "SW"; // Werkt niet
  }  

  if(windDirection > 3000 && windDirection < 3100) {
    readableWindDirection = "W";
  }
    
  if(windDirection > 3100 && windDirection < 3300) {
    readableWindDirection = "NW";
  }     
}

